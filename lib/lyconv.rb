#
# $Id: lyconv.rb 1447 2011-05-08 15:06:51Z nicb $
#
$:.unshift(File.dirname(__FILE__)) unless
  $:.include?(File.dirname(__FILE__)) || $:.include?(File.expand_path(File.dirname(__FILE__)))

module Lyconv
  VERSION = '0.0.1'

  EXT_LIB_PATH = 'extensions' unless defined?(EXT_LIB_PATH)
  LYCONV_LIB_PATH = 'lyconv' unless defined?(LYCONV_LIB_PATH)

  %w(
    integer
    string
  ).each { |f| require File.join(EXT_LIB_PATH, f) }

  %w(
    parser
    cli
  ).each { |f| require File.join(LYCONV_LIB_PATH, f) }

end
