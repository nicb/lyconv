#
# $Id: integer.rb 1447 2011-05-08 15:06:51Z nicb $
#

require 'rational'

class Integer

  def power_of_two?
    sz = self.size * 8
    cnt = 0
    0.upto(sz-1) { |n| cnt += 1 if self[n] == 1 }
    cnt == 1
  end

  def nearest_lower_power_of_two
    sz = self.size * 8
    res = 1
    0.upto(sz-1) do
      |n|
      pt = 2**n
      break if pt >= self
      res = pt
    end
    res
  end

end
