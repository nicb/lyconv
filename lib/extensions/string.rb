# frozen_string_literal: true

class String

  #
  # +characters_all_the_same?+
  #
  def characters_all_the_same?
    ref = self[0]
    result = self =~ /^#{ref}*$/
    return result ? true : false
  end

end
