#
# $Id: parser.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Lyconv

  PARSER_PATH = File.join(File.dirname(__FILE__), 'parser') unless defined?(PARSER_PATH)

  %w(
    globals
    line
  ).each { |f| require File.join(PARSER_PATH, f) }

end
