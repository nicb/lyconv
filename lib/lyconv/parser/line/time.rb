#
# $Id: time.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Lyconv

  module Parser

    module Line

      class Time

        attr_reader :num, :den, :r

        def initialize(n, d)
          @num = n
          @den = d
	  @r = Rational(self.num, self.den)
        end

        def tuplet?
          !self.den.power_of_two?
        end

	def to_ly
	  result = ''
	  if tuplet?
	  else
	  end
	  return result
	end

      private

        def decompose
	  result = []
	  start = self.r
	  while start.numerator > 1
	    part = Rational(1, self.r.denominator)
            result << part
	    start -= part
	    if start.numerator == 1
	    	start = 1.upto(start).map { Rational(1, 1) }
	    	result.unshift(start).flatten!
	    end
	  end
	  return result
	end

      end

    end

  end

end
