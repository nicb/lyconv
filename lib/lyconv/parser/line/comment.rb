#
# $Id: comment.rb 1447 2011-05-08 15:06:51Z nicb $
#

module Lyconv

  module Parser

    module Line

	    class Comment < Base
	
	      def to_s
	        "%% %s" % self.line.sub(/^#\s+/, '')
	      end
	
	      def parse
	        self.to_s
	      end
	
	    end

    end

  end

end
