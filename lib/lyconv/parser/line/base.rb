#
# $Id: base.rb 1447 2011-05-08 15:06:51Z nicb $
#
module Lyconv

  module Parser

    module Line

        class Base

	  class PureVirtualMethod < StandardError; end

	  class ParseError < StandardError; end

          NOTE_REGEXP = Regexp.compile(/^(\d+\.?\d*:){2}[a-g][f#]*[\+\-0-9]/)
    
          attr_reader :line
    
          def initialize(l)
            @line = l
          end

	  def to_ly
	    raise PureVirtualMethod, "Lyconv::Parser::Line::Base#to_ly pure virtual method called"
	  end
    
          class <<self
    
            def scan(line, cnt)
              line.chomp!
              lgi = Lyconv::Globals.instance
              ln = lgi.line_number = cnt
              file = lgi.cur_file
              out = case line
                  when /^#/        then Comment.new(line)
                  when /:tempo:/   then Tempo.new(line)
                  when /:ratio:/   then Ratio.new(line)
                  when /^=+/       then BarEnding.new(line)
                  when NOTE_REGEXP then Note.new(line)
                  else raise ParseError, "#{file} - #{ln}: unrecognized line \"#{line}\" while parsing"
                  end
              lgi.parse_stack << out
              return out
            end
    
          end
    
        end

    end

  end

end
