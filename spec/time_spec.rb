# frozen_string_literal: true

RSpec.describe Lyconv::Parser::Line::Time do

  before do
    @mat = Dir.glob(File.join(['..'] * 2, 'sections', 's??.dat')).sort
    @num, @den = 3, 4
    @times = {
      '3/4' => [ Rational(1, 2), Rational(1, 4) ],
      '1/2' => [ Rational(1, 2) ],
      '5/4' => [ Rational(1, 1), Rational(1, 4) ],
      '7/2' => [ Rational(1, 1), Rational(1, 1), Rational(1, 1), Rational(1, 2) ],
      '4/32' => [ Rational(1, 8) ],
      '6/32' => [ Rational(1, 8), Rational(1, 16) ],
    }
  end

  it 'can be created' do
    expect(Lyconv::Parser::Line::Time.new(@num, @den)).not_to be_nil
  end

  it 'has a working (private) decompose method' do
    @times.each do
      |key, should_be|
      num, den = key.split('/').map { |n| n.to_i }
      t = Lyconv::Parser::Line::Time.new(num, den)
byebug
      expect((res = t.send(:decompose))).to(eq(should_be), "time sig #{key} returned #{res.to_s} instead of #{should_be.to_s}")
    end
  end

end
