# frozen_string_literal: true

RSpec.describe Lyconv::Parser::Line::Note do

  before do
    @mat = Dir.glob(File.join(['..'] * 2, 'sections', 's??.dat')).sort
    @line = '10:32:f2:3:32:50'
    @correct_note_names = { 'd#2' => 'dis,', 'b-1' => 'b,,,,', 'e2' => 'e,', 'd##4' => "disis'", 'aff6' => "aeses'''" }
    @incorrect_note_names = [ 'd#', 'nf3', 'a#f-1' ]
  end

  it 'can be created' do
    expect(Lyconv::Parser::Line::Note.new(@line)).not_to be_nil
  end

  it 'has a working (private) note_name_to_ly method' do
    expect((n = Lyconv::Parser::Line::Note.new(@line))).not_to be_nil
    @correct_note_names.each do
      |nname, nexpected|
      n.note = nname
      expect((result = n.send(:note_name_to_ly))).to(eq(nexpected), "#{result} != #{nexpected}")
    end
  end

  it 'has a working (private) note_name_to_ly which will catch errors' do
    expect((n = Lyconv::Parser::Line::Note.new(@line))).not_to be_nil
    @incorrect_note_names.each do
      |nname|
      n.note = nname
      expect { n.send(:note_name_to_ly) }.to(raise_error(Lyconv::Parser::Line::NoteNameError), "--> #{nname}")
    end
  end

  it 'parses correctly all the possible notes' do
    @mat.each do
      |sect|
      File.open(sect, 'r') do
        |f|
        Lyconv::Globals.instance.cur_file = f.path
        cnt = 0
        while(line = f.gets)
          cnt += 1
          obj = Lyconv::Parser::Line::Base.scan(line, cnt)
	  expect(obj.to_ly).to be_truthy if obj.is_a?(Lyconv::Parser::Line::Note)
        end
        expect(cnt > 0).to(be(true), "#{cnt}")
      end
    end
  end

end
