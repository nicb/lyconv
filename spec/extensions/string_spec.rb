# frozen_string_literal: true

RSpec.describe "String extensions" do

  before do
    @true_strings = [ '###', 'fff', 'aaa', '222', '#', 'f', '2' ]
    @false_strings = [ '#f#f', 'f#f#', '23' ]
  end

  it 'has a working characters_all_the_same? method' do
    @true_strings.each { |s| expect(s.characters_all_the_same?).to be true }
    @false_strings.each { |s| expect(s.characters_all_the_same?).to be false }
  end
  
end
