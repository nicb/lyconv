# frozen_string_literal: true

RSpec.describe "Integer extensions" do

  before do
    @pows_of_two = 0.upto(31).map { |n| 2**n }
    @non_pows_of_two = 0.upto(16385).map do
      	|n|
        n unless @pows_of_two.index(n)
    end.compact
    @low_pots = { 2 => [3], 4 => [5, 6, 7], 8 => [9, 10, 11, 12, 13, 14, 15], 16 => [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31] }
  end

  it 'has a working power_of_two? method' do
    @pows_of_two.each do
      |p|
      expect(p.power_of_two?).to(be(true), "#{p}")
    end
    @non_pows_of_two.each do
      |p|
      expect(p.power_of_two?).to(be(false), "#{p}")
    end
  end
  
  it 'has a working nearest_lower_power_of_two method' do
    @low_pots.each do
      |pow, values|
      values.each do
        |val|
	expect(val.nearest_lower_power_of_two).to(eq(pow), "#{val}")
      end
    end
  end

end
